import React from 'react';
import AppBar from 'material-ui/AppBar';



const MaterialHeader = () => (
  <AppBar
    title="TodoApp"
    iconClassNameRight="muidocs-icon-navigation-expand-more"
  />
);

export default MaterialHeader;
